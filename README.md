# Compile pytroch from source

Building PyTorch from source reduced training time dramatically since it has better supports for Automatic Mixed Precision(AMP). We use CUDA 11.1 in this article. 

 
reference from:[https://medium.com/repro-repo/build-pytorch-from-source-on-ubuntu-18-04-1c5556ca8fbf](https://medium.com/repro-repo/build-pytorch-from-source-on-ubuntu-18-04-1c5556ca8fbf)

Make sure you are using the same conda environment for the rest of the article.

```
conda create --name pytorch-build
conda activate pytorch-build # or `source activate pytorch-build`
conda install astunparse numpy ninja pyyaml mkl mkl-include setuptools cmake cffi typing_extensions future six requests dataclasses
conda install -c pytorch magma-cuda111
```

Then, we want to tell CMake (building tool) where to put the resulting files.

```
export CMAKE_PREFIX_PATH="$HOME/anaconda3/envs/pytorch-build"
```

As a precaution against older Anaconda symbolic linking mistakes, we temporarily rename its compatibility linker, before renaming it back later:
```
cd ~/anaconda3/envs/pytorch-build/compiler_compat
mv ld ld-old
```

Now, for some reason, PyTorch cannot find OpenMP out of the box, so we have to explicitly install OpenMP, a library for better CPU multi-threading:

```
sudo apt-get install libomp-dev
```

Now that we’ve done all the prep work, download PyTorch code into your home folder for convenience.
```
cd ~ (or you where ever you wants pytorch be installed)
git clone --recursive https://github.com/pytorch/pytorch
```

If you have multiple cuda versions on your machine and you want to specify which one (or your system is failing to locate nvcc, you must specify the path for nvcc, CUDA_NVCC_EXECUTABLE. For example, if just doing nvcc doesn’t work, and you want to use CUDA 11.1(recommended for new nvidia cards), do:

```
export CUDA_NVCC_EXECUTABLE="/usr/local/cuda-11.1/bin/nvcc"
export CUDA_HOME="/usr/local/cuda-11.1"
export CUDNN_INCLUDE_PATH="/usr/local/cuda-11.1/include/"
export CUDNN_LIBRARY_PATH="/usr/local/cuda-11.1/lib64/"
export LIBRARY_PATH="/usr/local/cuda-11.1/lib64"
```


(option)If you have tried to install pytorch from source before, try to clean up previous setting:
```
git clean -xdf
python setup.py clean
git submodule sync
git submodule deinit -f .
git submodule update --init --recursive
```

Anyway, you can now build and install the library with the following steps:
#(you can skip)export USE_CUDA=1 USE_CUDNN=1 USE_MKLDNN=1

```
cd ~/pytorch
python setup.py install
```

Now remember to rename back the Anaconda compiler linker:
```
cd ~/anaconda3/envs/pytorch-build/compiler_compat
mv ld-old ld
```

You are done!
